#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    m_deviceDiscoveryAgent = 0;
    this->startDiscovery();
}

Widget::~Widget()
{
    delete ui;
}

void Widget::startDiscovery()
{
    m_deviceDiscoveryAgent = new QBluetoothDeviceDiscoveryAgent(this);

    connect(m_deviceDiscoveryAgent, SIGNAL(deviceDiscovered(const QBluetoothDeviceInfo&)),
            this, SLOT(addDevice(const QBluetoothDeviceInfo&)));

    connect(m_deviceDiscoveryAgent, SIGNAL(error(QBluetoothDeviceDiscoveryAgent::Error)),
            this, SLOT(deviceScanError(QBluetoothDeviceDiscoveryAgent::Error)));

    connect(m_deviceDiscoveryAgent, SIGNAL(finished()), this, SLOT(scanFinished()));

    m_deviceDiscoveryAgent->start();
}

void Widget::addDevice(const QBluetoothDeviceInfo &device)
{
    if (device.coreConfigurations() & QBluetoothDeviceInfo::LowEnergyCoreConfiguration) {
        qDebug() << device.name() << endl;
        sensorObject *ob =  new sensorObject(device);
        ui->verticalLayout->insertWidget(ob->count , ob);
    }
}

void Widget::deviceScanError(QBluetoothDeviceDiscoveryAgent::Error)
{

}

void Widget::scanFinished()
{

}





