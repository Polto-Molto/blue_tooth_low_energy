#include "sensorobject.h"
int sensorObject::count = 0;

sensorObject::sensorObject(const QBluetoothDeviceInfo device,QWidget *parent) : QWidget(parent)
{
    foundService =false;
    m_control = 0;
    m_services.clear();
    device_info = device;
    count++;
    QString str;
    this->setFixedSize(148,148);
    main_button = new QPushButton(this);
    main_button->setText(device.name());
    main_button->setGeometry(0,0,this->width(),this->height());
    connect(this->main_button , SIGNAL(clicked(bool)),this,SLOT(connectToDevice()));
}

void sensorObject::on_main_button_clicked()
{
    qDebug() << " Try connecting device ..." << endl;
    this->connectToDevice();
}

void sensorObject::connectToDevice()
{
    qDebug() << " Try connecting device ..." << endl;
    m_services.clear();
    m_control = new QLowEnergyController(device_info, this);

    connect(m_control, SIGNAL(serviceDiscovered(QBluetoothUuid)),
            this, SLOT(serviceDiscovered(QBluetoothUuid)));

    connect(m_control, SIGNAL(discoveryFinished()),
            this, SLOT(serviceScanDone()));
/*
    connect(m_control, SIGNAL(error(QLowEnergyController::Error)),
            this, SLOT(controllerError(QLowEnergyController::Error)));
*/
    connect(m_control, SIGNAL(connected()),
            this, SLOT(deviceConnected()));

    connect(m_control, SIGNAL(disconnected()),
            this, SLOT(deviceDisconnected()));

    m_control->connectToDevice();
}

void sensorObject::serviceDiscovered(const QBluetoothUuid &gatt)
{
    QLowEnergyService* serv = m_control->createServiceObject(gatt);
    m_services.append(serv);
    qDebug() << serv->serviceName();
}

void sensorObject::serviceScanDone()
{
    foreach(QLowEnergyService *serv , m_services)
    {
        const QList<QLowEnergyCharacteristic> chars = serv->characteristics();
        foreach (const QLowEnergyCharacteristic &ch, chars) {
        qDebug() << ch.name();
        }
        connect(serv, SIGNAL(stateChanged(QLowEnergyService::ServiceState)),
                this, SLOT(serviceStateChanged(QLowEnergyService::ServiceState)));

        connect(serv, SIGNAL(characteristicChanged(QLowEnergyCharacteristic,QByteArray)),
                this, SLOT(updateHeartRateValue(QLowEnergyCharacteristic,QByteArray)));

        connect(serv, SIGNAL(descriptorWritten(QLowEnergyDescriptor,QByteArray)),
                this, SLOT(confirmedDescriptorWrite(QLowEnergyDescriptor,QByteArray)));

        serv->discoverDetails();
    }

 //   if (!m_service) {
        //setMessage("Heart Rate Service not found.");
   //     return;
 //   }
/*
    const QList<QLowEnergyCharacteristic> chars = m_service->characteristics();
    foreach (const QLowEnergyCharacteristic &ch, chars) {
    qDebug() << ch.name();
    }
    connect(m_service, SIGNAL(stateChanged(QLowEnergyService::ServiceState)),
            this, SLOT(serviceStateChanged(QLowEnergyService::ServiceState)));

    connect(m_service, SIGNAL(characteristicChanged(QLowEnergyCharacteristic,QByteArray)),
            this, SLOT(updateHeartRateValue(QLowEnergyCharacteristic,QByteArray)));

    connect(m_service, SIGNAL(descriptorWritten(QLowEnergyDescriptor,QByteArray)),
            this, SLOT(confirmedDescriptorWrite(QLowEnergyDescriptor,QByteArray)));

    m_service->discoverDetails();*/
}

void sensorObject::deviceConnected()
{
    m_control->discoverServices();
}

void sensorObject::deviceDisconnected()
{
    qWarning() << "Remote device disconnected";
}

void sensorObject::serviceStateChanged(QLowEnergyService::ServiceState s)
{

}

void sensorObject::confirmedDescriptorWrite(QLowEnergyService::ServiceState s)
{

}

void sensorObject::updateHeartRateValue(const QLowEnergyCharacteristic &c,
                                     const QByteArray &value)
{

}
