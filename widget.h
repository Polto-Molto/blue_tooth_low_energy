#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <sensorobject.h>
#include <QBluetoothDeviceDiscoveryAgent>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
    void startDiscovery();

public slots:
    void addDevice(const QBluetoothDeviceInfo&);
    void deviceScanError(QBluetoothDeviceDiscoveryAgent::Error);
    void scanFinished();

private:
    Ui::Widget *ui;
    QBluetoothDeviceDiscoveryAgent *m_deviceDiscoveryAgent;
};

#endif // WIDGET_H
