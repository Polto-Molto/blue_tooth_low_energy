#-------------------------------------------------
#
# Project created by QtCreator 2017-01-18T15:58:22
#
#-------------------------------------------------

QT       += core gui bluetooth

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = tom_btle_sensors
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    sensorobject.cpp

HEADERS  += widget.h \
    sensorobject.h

FORMS    += widget.ui
