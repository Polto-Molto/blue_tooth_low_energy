#ifndef SENSOROBJECT_H
#define SENSOROBJECT_H

#include <QObject>
#include <QWidget>
#include <QPushButton>
#include <QBluetoothDeviceDiscoveryAgent>
#include <QLowEnergyController>
#include <QDateTime>

class sensorObject : public QWidget
{
    Q_OBJECT
public:
    explicit sensorObject(const QBluetoothDeviceInfo ,QWidget *parent = 0);
    static int count;

signals:

public slots:
    void on_main_button_clicked();

    void connectToDevice();

    void serviceDiscovered(const QBluetoothUuid &gatt);

    void serviceScanDone();

    void deviceConnected();

    void deviceDisconnected();

    void serviceStateChanged(QLowEnergyService::ServiceState s);
    void confirmedDescriptorWrite(QLowEnergyService::ServiceState s);
    void updateHeartRateValue(const QLowEnergyCharacteristic &c,
                                         const QByteArray &value);

private:
    QPushButton *main_button;
    QDateTime m_start;
    QLowEnergyController *m_control;
    QList<QLowEnergyService *> m_services;
    QBluetoothDeviceInfo device_info;
    bool foundService;
};

#endif // SENSOROBJECT_H
